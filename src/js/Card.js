import classNames from "classnames";
import EventEmitter from "eventemitter3";
import Notification from "./Notification.js"

export default class Card extends EventEmitter {
  static get events() {
    return {
      ADD_TO_CART: "add_to_cart",
    };
  }

  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
    };
  }

  constructor({ type, price }) {
    super();
    this.notify= new Notification(type,price);
    console.log(this.notify);
    this.on('add_to_cart',(type,price)=>this.notify.render(type,price))
    this._type = type;
    this._price = price
    this.container = document.createElement("div");
    this.container.classList.add("card-container");
  }

  render() {
    const template = `
<div data-price="${this._price}" class="card type-${this._type} ${classNames({
      "is-danger": this._type === Card.types.HAWAIIAN,
    })}">
  <div class="emoji">🍕</div>
  <span class="type">${this._type}</span>
</div>
    `;

    this.container.innerHTML = template;
    this.container.addEventListener("click", () => {
      console.log(this._price,this._type);
      
      this.emit(Card.events.ADD_TO_CART, {
        type: this._type,
        price: this._price,
      });
    });
  }
}
