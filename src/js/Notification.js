import classNames from 'classnames';
import {formatCurrency} from './utils.js'
 


export default class Notification {
  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
      
    };
  }

  constructor(type,price) {
     this._type = type;
     this._price = price;
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");
  }

  empty() {
    console.log(document
      .querySelector('.notification-container'));
    document
    .querySelector('.notification-container').remove();
  }
render({price,type}) {
    console.log(price,type);
    const template = `
<div class="notification ${classNames({
  "is-danger": type === Notification.types.HAWAIIAN,
})} type-${type}">
  <button class="delete"></button>
  🍕 <span class="type">${type}</span> <span class="price">
  ${formatCurrency(price)}</span> has been added to your order.
</div>
    `;
console.log(template,this.container);
    this.container.innerHTML =template;

    
    // console.log(document.querySelector(`.type-${type}`).innerHTML='')
    document.querySelector('.main').appendChild(this.container);
    document.querySelector('.delete').addEventListener('click',()=>{this.empty()});
    }
}
